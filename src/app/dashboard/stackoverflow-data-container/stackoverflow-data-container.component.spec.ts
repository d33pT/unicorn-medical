import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StackoverflowDataContainerComponent } from './stackoverflow-data-container.component';

describe('StackoverflowDataContainerComponent', () => {
  let component: StackoverflowDataContainerComponent;
  let fixture: ComponentFixture<StackoverflowDataContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StackoverflowDataContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StackoverflowDataContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
