import {Component, Input, OnInit} from '@angular/core';
import {InterfaceStackData} from '../interface-stack-data';

@Component({
  selector: 'app-stackoverflow-data-container',
  templateUrl: './stackoverflow-data-container.component.html',
  styleUrls: ['./stackoverflow-data-container.component.scss']
})
export class StackoverflowDataContainerComponent implements OnInit {

  @Input() stackOverFlowData: InterfaceStackData;

  constructor() { }

  ngOnInit() {
  }

}
