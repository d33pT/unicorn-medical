import {Component, Input, OnInit} from '@angular/core';
import {SearchService} from '../../core/services/search.service';
import {InterfaceStackData} from '../interface-stack-data';

@Component({
  selector: 'app-stackoverflow-data',
  templateUrl: './stackoverflow-data.component.html',
  styleUrls: ['./stackoverflow-data.component.scss']
})
export class StackoverflowDataComponent implements OnInit {

  @Input() topic: string;

  stackOverFlowList: InterfaceStackData [];

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchService.search(this.topic).subscribe(data => this.stackOverFlowList = data['items']);
  }

}
