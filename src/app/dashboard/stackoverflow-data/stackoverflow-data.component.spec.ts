import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StackoverflowDataComponent } from './stackoverflow-data.component';

describe('StackoverflowDataComponent', () => {
  let component: StackoverflowDataComponent;
  let fixture: ComponentFixture<StackoverflowDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StackoverflowDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StackoverflowDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
